	#HW: C to ASM -O0
	# Ayush Shresth
	# Emanuel Navarro-Ortiz
	# Nguyen Chau
	# Due Monday Oct 31st

	sum2:						# sum2 label
	sub     sp, sp, #8			# preserve 8 bytes of memory in the stack
	str     r0, [sp, #4]			# push register r0 to the stack
	str     r1, [sp]			# push register r1 to the stack
	ldr     r0, [sp, #4]			# r0 = argument variable 1
	ldr     r1, [sp]			# r1 = argument variable 2
	add     r0, r0, r1			# r0 = r0 + r1
	add     sp, sp, #8			# clear  8 bytes of memory  in the stack
	bx      lr				# treat the next instruction as a return address


	print_the_value:			# print_the_value label
	push    {r11, lr}			# push register r11 to the stack, and lr to preserve the return address
	mov     r11, sp			        # move the stack pointer to register r11
	sub     sp, sp, #8			# preserve 8 bytes of memory in the stack
	str     r0, [sp, #4]			# push register r0 to the stack
	ldr     r1, [sp, #4]			# r1 = argument variable 1
	ldr     r0, .LCPI1_0			# load register r0 to a string
	bl      printf				# call branch label printf to print the value
	mov     sp, r11			        # move register 11 value to stack pointer
	pop     {r11, lr}			# pop register r11 and the return address out of stack
	bx      lr				# treat the next instruction as a return address


	.LCPI1_0:				# LCPI1_0 label
	.long   .L.str			        # loads a 32 int to a string .L.str

	entry_point:					# entry_point label
	push    {r11, lr}			# push register r11 to the stack, and lr to preserve the return address
	mov     r11, sp			        # move the stack pointer to register r11
	sub     sp, sp, #16			# preserve 16 bytes of memory in the stack
	bl      rand				# call branch label rand to generates a random number and store into r0
	str     r0, [sp, #8]			# push register r0 to the stack
	bl      rand				# call branch label rand to generates a random number and store into r0
	str     r0, [sp, #4]			# push register r0 to the stack
	ldr     r0, [sp, #4]			# r0 = argument variable 1, the second push stack
	ldr     r1, [sp, #8]			# r1 = argument variable 2, the first push stack
	bl      sum2				# call branch label sum2 to calculate the sum of 2 arguments
	str     r0, [sp]			# push the return value of sum2 to r0 to the stack
	ldr     r0, [sp]			# r0 = sum2 return value
	bl      print_the_value		        # call branch label print_the_value to print the value of r0
	ldr     r0, [r11, #-4]			# pop register r0 out of the stack
	mov     sp, r11		 	        # move register 11 value to stack pointer
	pop     {r11, lr}			# pop register r11 and the return address out of stack
	bx      lr				# treat the next instruction as a return address


	.L.str:					# L.str label
	.asciz  "%d"			        # initializing a string for a 32 bit signed integer

		
